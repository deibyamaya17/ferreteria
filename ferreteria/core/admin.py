
from core.models import Cliente
from django.contrib import admin

# Register your models here.

# admin.site.register(Cliente)
# admin.site.register(Producto)

class ClienteTypeAdmin(admin.ModelAdmin):

    list_display = ['codigo', 'run', 'apellido_paterno',
                'apellido_materno']
    ordering = ['codigo']



admin.site.register(Cliente, ClienteTypeAdmin)
from django.urls import path
from .views import home, registrocliente, loginvista, loginproveedor, comprarproducto

urlpatterns = [
    path('', home, name="home"),
    path('registrocliente/', registrocliente, name="registrocliente"),
    path('loginvista/', loginvista, name="loginvista"),
    path('loginproveedor/', loginproveedor, name="loginproveedor"),
    path('comprarproducto/', comprarproducto, name="comprarproducto"),
]

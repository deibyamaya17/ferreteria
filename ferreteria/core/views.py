from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'core/home.html')

def registrocliente(request):
    return render(request, 'core/registrocliente.html')

def loginvista(request):
    return render(request, 'core/loginvista.html')  

def loginproveedor(request):
    return render(request, 'core/loginproveedor.html')   

def comprarproducto(request):
    return render(request, 'core/comprarproducto.html' )